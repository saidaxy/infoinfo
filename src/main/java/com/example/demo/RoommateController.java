package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController

@RequestMapping("/roommate/info")

public class RoommateController {





    @GetMapping("/{userId}")

    public UserRoommate getRoommateByUserId(

            @PathVariable("userId") String userId) {



        List<Roommate> userRoommateList =  Arrays.asList(

                new Roommate("1", "Roommate 1", "Female", "Desc 1"),

                new Roommate("2", "Roommate 2", "Female", "Desc 2"));



        UserRoommate userRoommate = new UserRoommate(userRoommateList);



        return userRoommate;

    }

}
