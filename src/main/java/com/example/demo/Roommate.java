package com.example.demo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Roommate {
    public Roommate() {

    }



    public Roommate(String id, String name, String gender, String description) {

        this.id = id;

        this.name = name;

        this.gender = gender;

        this.description = description;

    }



    private String id;

    private String name;

    private String gender;

    private String description;
}
