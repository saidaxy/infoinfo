package com.example.demo;

import lombok.Getter;

import lombok.Setter;



import java.util.List;



@Getter

@Setter

public class UserRoommate {



    private List<Roommate> userRoommates;



    public UserRoommate() {

    }



    public UserRoommate(List<Roommate> userRoommates) {

        this.userRoommates = userRoommates;

    }

}
